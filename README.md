Stargazers App
=====================================

This sample shows how to use github api to find out all users interested in a github repository.
 
Pre-requisites
--------------

- Android SDK 26

Getting Started
---------------

This sample uses the Gradle build system. To build this project, use the
"gradlew build" command or use "Import Project" in Android Studio.
That's it!

Screenshots
-----------

![Phone](./screen.png "On a phone")

Library
-------
- Anko Common 0.10.2
- Retrofit 2.3.0
- Converter Moshi 2.0.0
- Picasso 2.5.2
