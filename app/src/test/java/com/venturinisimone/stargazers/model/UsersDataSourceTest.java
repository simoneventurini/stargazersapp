package com.venturinisimone.stargazers.model;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

import kotlin.Pair;

/**
 * Created by sventurini on 19/11/2017.
 */

public class UsersDataSourceTest {

    @Test
    public void testGetFirstUsersPage() {
        UsersDataSource dataSourceInterface = Mockito.mock(UsersDataSource.class);
        dataSourceInterface.getFirstUsersPage("simoneventurini", "HorizontalScrollView", new UsersDataSource.LoadUsersCallback() {
            @Override
            public void onUsersLoaded(@NotNull Pair<? extends List<User>, String> response) {
                Assert.assertEquals("", response.component2());
            }
        });
    }
}
