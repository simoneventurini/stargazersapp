package com.venturinisimone.stargazers.apiclient;

import org.junit.Assert;
import org.junit.Test;

import okhttp3.Headers;

/**
 * Created by sventurini on 20/11/2017.
 */

public class UtilsTest {

    @Test
    public void parseNextPageLink() {
        Utils utils = new Utils();
        Headers headers = new Headers.Builder()
                .add("Link", "<https://api.github.com/repositories/892275/stargazers?page=2>; rel=\"next\", <https://api.github.com/repositories/892275/stargazers?page=832>; rel=\"last\"")
                .build();


        String next = utils.parseNextPageLink(headers);
        Assert.assertEquals(next, "https://api.github.com/repositories/892275/stargazers?page=2");
    }

    @Test
    public void parseNextPageLinkNoNext() {
        Headers headers = new Headers.Builder()
                .add("Link", "<https://api.github.com/repositories/892275/stargazers?page=2>; rel=\"---\", <https://api.github.com/repositories/892275/stargazers?page=832>; rel=\"last\"")
                .build();

        Utils utils = new Utils();
        String next = utils.parseNextPageLink(headers);
        Assert.assertEquals(next, "");
    }

    @Test
    public void parseNextPageLinkEmpty() {
        Headers headers = new Headers.Builder()
                .build();

        Utils utils = new Utils();
        String next = utils.parseNextPageLink(headers);
        Assert.assertEquals(next, "");
    }
}
