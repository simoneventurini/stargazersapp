package com.venturinisimone.stargazers.presenter

/**
 * Created by sventurini on 18/11/2017.
 */
interface BasePresenter {

    fun start()
}
