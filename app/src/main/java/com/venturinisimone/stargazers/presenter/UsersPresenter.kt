package com.venturinisimone.stargazers.presenter

import com.venturinisimone.stargazers.model.User
import com.venturinisimone.stargazers.model.UsersDataSource

/**
 * Created by sventurini on 18/11/2017.
 */
class UsersPresenter(view: UsersContract.View, private var dataSource: UsersDataSource): UsersContract.Presenter, UsersDataSource.LoadUsersCallback  {

    private var mView: UsersContract.View = view
    private var mNextPage: String = ""
    private var isLoading = false

    init {
        mView.setPresenter(this)
    }

    override fun start() {
    }

    override fun loadUsers(owner: String, repo: String) {
        if (isLoading) return
        isLoading = true
        mView.showLoading()
        dataSource.getFirstUsersPage(owner, repo, this)
    }

    override fun loadNextPage() {
        if (isLoading || mNextPage.isEmpty()) return
        isLoading = true
        dataSource.getNextUsersPage(mNextPage, this)
    }

    override fun onUsersLoaded(response: Pair<List<User>?, String>) {
        isLoading = false
        mNextPage = response.second

        if (response.first != null) {
            if (mView.isActive()) {
                mView.showUsers(ArrayList(response.first), mNextPage == "")
            }

        } else {
            mView.noUsers()
        }
    }
}
