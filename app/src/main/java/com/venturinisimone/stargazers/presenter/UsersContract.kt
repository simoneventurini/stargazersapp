package com.venturinisimone.stargazers.presenter

import com.venturinisimone.stargazers.model.User
import com.venturinisimone.stargazers.view.BaseView

/**
 * Created by sventurini on 18/11/2017.
 */
interface UsersContract {

    interface View: BaseView<Presenter> {
        fun showLoading()
        fun noUsers()
        fun showUsers(users: ArrayList<User>, isCompleted: Boolean)
        fun isActive(): Boolean
    }

    interface Presenter: BasePresenter {
        fun loadUsers(owner: String, repo: String)
        fun loadNextPage()
    }
}
