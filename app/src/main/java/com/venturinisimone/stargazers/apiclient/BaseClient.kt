package com.venturinisimone.stargazers.apiclient

import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * Created by sventurini on 18/11/2017.
 */
abstract class BaseClient {

    companion object {
        @Volatile
        var BASE_URL: String = ""
    }

    protected fun <T> create(service: Class<T>): T {
        val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .build()

        return  retrofit.create(service)
    }
}
