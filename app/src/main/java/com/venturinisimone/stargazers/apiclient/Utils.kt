package com.venturinisimone.stargazers.apiclient

/**
 * Created by sventurini on 19/11/2017.
 */
class Utils {

    fun parseNextPageLink(headers: okhttp3.Headers): String {
        var nextlink = ""
        val firstElement = headers.get("Link")?.split(",") ?: return nextlink
        if (firstElement[0].contains("next")) {
            nextlink = firstElement[0].split(";")[0].removeSurrounding("<", ">")
        }
        return nextlink
    }
}