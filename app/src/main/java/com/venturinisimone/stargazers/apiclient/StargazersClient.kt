package com.venturinisimone.stargazers.apiclient

import com.venturinisimone.stargazers.model.User

/**
 * Created by sventurini on 18/11/2017.
 */
class StargazersClient: BaseClient() {

    fun getUsers(owner: String, repo: String): Pair<List<User>?, String> {
        val service = create(StargazersClientInterface::class.java)
        val output = service.getStargazers(owner, repo).execute()
        val headers = output.headers()

        return Pair(output.body(), Utils().parseNextPageLink(headers))
    }

    fun getUsers(pageUrl: String): Pair<List<User>?, String> {
        val service = create(StargazersClientInterface::class.java)
        val output = service.getStargazers(pageUrl).execute()
        val headers = output.headers()

        return Pair(output.body(), Utils().parseNextPageLink(headers))
    }
}
