package com.venturinisimone.stargazers.apiclient

import com.venturinisimone.stargazers.model.User
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Url



/**
 * Created by sventurini on 18/11/2017.
 */
interface StargazersClientInterface {

    @GET("repos/{owner}/{repo}/stargazers")
    fun getStargazers(@Path("owner") owner: String, @Path("repo")repo: String): Call<List<User>>

    @GET
    fun getStargazers(@Url url: String): Call<List<User>>
}
