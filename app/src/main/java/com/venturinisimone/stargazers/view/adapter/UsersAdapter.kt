package com.venturinisimone.stargazers.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.venturinisimone.stargazers.R
import com.venturinisimone.stargazers.model.User
import com.venturinisimone.stargazers.widget.BezelImageView

/**
 * Created by sventurini on 18/11/2017.
 */
class UsersAdapter(context: Context, private var listener: OnScrolled): RecyclerView.Adapter<UsersAdapter.ViewHolder>() {

    interface OnScrolled {
        fun requestOtherItems()
    }

    private var mContext: Context = context
    private var users: ArrayList<User> = ArrayList()
    private var showLoading = false

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.listitem_user, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val user = users[position]
        Picasso.with(mContext)
                .load(user.avatar_url)
                .placeholder(R.drawable.ic_user_male)
                .fit()
                .centerCrop()
                .into(holder?.imgImage)
        holder?.txtName?.text = user.login

        if (position == itemCount - 1 && showLoading) {
            holder?.loading!!.visibility = View.VISIBLE
            listener.requestOtherItems()

        } else {
            holder?.loading!!.visibility = View.GONE

        }
    }

    override fun getItemCount(): Int {
        return users.size
    }

    fun addUsers(users: ArrayList<User>) {
        this.users.addAll(users)
        notifyDataSetChanged()
    }

    fun clearAllUsers() {
        users.clear()
        notifyDataSetChanged()
    }

    fun showLoading(value: Boolean) {
        showLoading = value
        notifyItemChanged(itemCount - 1)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgImage: BezelImageView = itemView.findViewById(R.id.imgUser)
        var txtName: TextView = itemView.findViewById(R.id.txtUserName)
        var loading: View = itemView.findViewById(R.id.loading)
    }
}
