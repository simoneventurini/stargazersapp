package com.venturinisimone.stargazers.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.venturinisimone.stargazers.R
import com.venturinisimone.stargazers.presenter.UsersContract
import com.venturinisimone.stargazers.view.adapter.UsersAdapter
import com.venturinisimone.stargazers.model.User

/**
 * Created by sventurini on 18/11/2017.
 */
class UsersFragment : Fragment, UsersContract.View, UsersAdapter.OnScrolled {


    private lateinit var mUsersPresenter: UsersContract.Presenter
    private lateinit var mAdapter: UsersAdapter

    constructor() : super()

    override fun setPresenter(presenter: UsersContract.Presenter) {
        mUsersPresenter = presenter
    }

    companion object {
        fun newInstance(): UsersFragment = UsersFragment()
    }

    private lateinit var mRecyclerView :RecyclerView
    private lateinit var mLoading: View
    private lateinit var mboxHint :View

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var rowView: View? = view
        if (rowView == null) {
            rowView = inflater?.inflate(R.layout.fragment_users, container, false)
        }

        mLoading = rowView!!.findViewById(R.id.loading)
        mboxHint = rowView.findViewById(R.id.boxHint)

        mAdapter = UsersAdapter(activity, this)
        mRecyclerView = rowView.findViewById(R.id.recyclerView)

        mRecyclerView.layoutManager = LinearLayoutManager(activity)
        mRecyclerView.adapter = mAdapter

        return rowView
    }

    override fun onResume() {
        super.onResume()
        mUsersPresenter.start()
    }

    override fun isActive(): Boolean {
            return isAdded
    }


    override fun showLoading() {
        mAdapter.clearAllUsers()
        mLoading.visibility = View.VISIBLE
        mboxHint.visibility = View.GONE
    }

    override fun noUsers() {
        mLoading.visibility = View.GONE
        mboxHint.visibility = View.VISIBLE
        Toast.makeText(activity, getString(R.string.no_user_found), Toast.LENGTH_SHORT).show()
    }

    override fun showUsers(users: ArrayList<User>, isCompleted: Boolean) {
        if (mLoading.visibility == View.VISIBLE) mLoading.visibility = View.GONE
        mAdapter.addUsers(users)
        mAdapter.showLoading(!isCompleted)
    }

    override fun requestOtherItems() {
        mUsersPresenter.loadNextPage()
    }
}
