package com.venturinisimone.stargazers.view

import com.venturinisimone.stargazers.presenter.BasePresenter

/**
 * Created by sventurini on 18/11/2017.
 */
interface BaseView<in T : BasePresenter> {

    fun setPresenter(presenter: T)
}
