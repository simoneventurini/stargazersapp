package com.venturinisimone.stargazers.model

/**
 * Created by sventurini on 18/11/2017.
 */
class User (
    val id: String,
    val login: String,
    val avatar_url: String
)
