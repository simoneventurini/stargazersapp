package com.venturinisimone.stargazers.model

/**
 * Created by sventurini on 18/11/2017.
 */
interface UsersDataSource {

    interface LoadUsersCallback {
        fun onUsersLoaded(response: Pair<List<User>?, String>)
    }

    fun getFirstUsersPage(owner: String, repo: String, callback: LoadUsersCallback)
    fun getNextUsersPage(pageUrl: String, callback: LoadUsersCallback)
}
