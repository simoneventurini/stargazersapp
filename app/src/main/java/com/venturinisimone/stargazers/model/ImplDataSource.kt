package com.venturinisimone.stargazers.model

import com.venturinisimone.stargazers.apiclient.StargazersClient
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

/**
 * Created by sventurini on 18/11/2017.
 */
class ImplDataSource : UsersDataSource {

    /**
     * No local storage. We always call remote api
     */
    override fun getFirstUsersPage(owner: String, repo: String, callback: UsersDataSource.LoadUsersCallback) {
        doAsync {
            val response = StargazersClient().getUsers(owner, repo)
            uiThread {
                callback.onUsersLoaded(response)
            }
        }
    }

    override fun getNextUsersPage(pageUrl: String, callback: UsersDataSource.LoadUsersCallback) {
        doAsync {
            val response = StargazersClient().getUsers(pageUrl)
            uiThread {
                callback.onUsersLoaded(response)
            }
        }
    }
}
