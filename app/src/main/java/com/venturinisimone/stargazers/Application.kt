package com.venturinisimone.stargazers

import android.support.multidex.MultiDexApplication
import com.venturinisimone.stargazers.apiclient.BaseClient

/**
 * Created by sventurini on 18/11/2017.
 */
class Application: MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        BaseClient.BASE_URL = getString(R.string.github_schema)
    }
}
