package com.venturinisimone.stargazers

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.Toast
import com.venturinisimone.stargazers.model.ImplDataSource
import com.venturinisimone.stargazers.presenter.UsersPresenter
import com.venturinisimone.stargazers.view.UsersFragment


class MainActivity : AppCompatActivity() {

    lateinit var mUsersPresenter: UsersPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var usersFragment = UsersFragment.newInstance()
        supportFragmentManager.beginTransaction().replace(R.id.frmLayout, usersFragment).commit()

        mUsersPresenter = UsersPresenter(usersFragment, ImplDataSource())
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (R.id.actionSearch == item?.itemId!!) {
            showChangeLangDialog()
        }
        return super.onOptionsItemSelected(item)
    }

    fun showChangeLangDialog() {
        val dialogBuilder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_search, null)
        dialogBuilder.setView(dialogView)

        val edtOwn = dialogView.findViewById<EditText>(R.id.editOwner)
        val edtRepo = dialogView.findViewById<EditText>(R.id.editRepo)

        dialogBuilder.setTitle(getString(R.string.dialog_title_search))
        dialogBuilder.setMessage(getString(R.string.dialog_message_search))
        dialogBuilder.setPositiveButton("Done") { dialog, whichButton ->
            if (checkInternetConnection()) {
                supportActionBar?.title = edtOwn.text.toString()
                supportActionBar?.subtitle = edtRepo.text.toString()
                mUsersPresenter.loadUsers(edtOwn.text.toString(), edtRepo.text.toString())

            } else {
                Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show()

            }
        }
        dialogBuilder.setNegativeButton("Cancel") { dialog, whichButton ->

        }
        val b = dialogBuilder.create()
        b.show()
    }

    fun checkInternetConnection(): Boolean {
        val conMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return (conMgr.activeNetworkInfo != null
                && conMgr.activeNetworkInfo.isAvailable
                && conMgr.activeNetworkInfo.isConnected)
    }
}
